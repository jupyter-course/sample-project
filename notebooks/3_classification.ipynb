{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Classifying the spectra\n",
    "\n",
    "After we [cleaned the spectra](2_preprocessing.ipynb), we are now ready to analyze them.\n",
    "For the sake of this example, we will merely use statistical tools to do this, while in a \"real\" project we would certainly use more knowledge about the spectral features than we do here.\n",
    "\n",
    "We will use [&rarr;k-means clustering](https://scikit-learn.org/stable/modules/clustering.html#k-means) (a method to find entries with similar features in a dataset) to categorise the spectra and [&rarr;principal component analysis](https://scikit-learn.org/stable/modules/decomposition.html#pca) to transform the data and visualize the clusters in two dimensions (`scikit-learn`).\n",
    "\n",
    "`pandas` will help us to easily group and average spectra by category, so we can have a look what differences the k-means algorithm found."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "\n",
    "from bokeh.io import output_notebook, save\n",
    "from bokeh.layouts import gridplot\n",
    "from bokeh.models import HoverTool\n",
    "from bokeh.plotting import figure, show\n",
    "from bokeh.palettes import Category20\n",
    "\n",
    "from itertools import combinations\n",
    "\n",
    "from matplotlib.colors import ListedColormap, hex2color\n",
    "\n",
    "from sklearn.cluster import KMeans\n",
    "from sklearn.decomposition import PCA\n",
    "from sklearn.preprocessing import scale\n",
    "\n",
    "from scripts.io import load_spectra\n",
    "from scripts.visualization import plot_line\n",
    "\n",
    "output_notebook()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wavelength, spectra = load_spectra(use_cleaned_data=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we unpack the spectral data (a 3-dimensional data cube of spectrum versus y position vs x position) into a flat table. We keep track of the spatial position of the spectra by including the two columns \"x\" and \"y\".\n",
    "\n",
    "Note that we are only looking at half the image, where the sunlight is reflected from Titan's surface (this makes the clustering easier):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spectra_df = pd.DataFrame([(i, j, *spectra[:, i, j]) for i in range(64) for j in range(32)])\n",
    "spectra_df.columns = ['x', 'y'] + [f'channel_{i+1}' for i in range(256)]\n",
    "spectra_df.index.name = 'spectrum_id'\n",
    "spectra_df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before feeding the spectral data into the k-means algorithm, we scale each channel to unit variance and a mean of zero. This is to make sure that each channel can contribute to the overall decision to what category a spectrum belongs. (The necessity of this step would be clearer if we would try to cluster a dataset of cars, say, where the different features like maximum speed or gas usage would have different scales and units. You have to \"normalize\" such datasets, otherwise one feature might dominate merely because of its unit of measure and large scale.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scaled_spectra = scale(spectra_df.values[:, 2:])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Find optimal number of clusters\n",
    "\n",
    "We have one important parameter to decide upon for our spectral clustering: the number of clusters.\n",
    "A simple way to deduce a reasonable number of clusters is the so called \"elbow method\". In this method, we basically plot how well the data fits the selected clusters versus the number of clusters. The `score` in the cells below is derived fom the distance of our data points to their nearest cluster centrum. When the number of clusters we selected matches the true number of clusters in the dataset, adding more clusters will not improve the score much. A plot of score versus number of clusters must thus level off at some point, and this indicates an appropriate number of clusters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# calculate score vs. number of clusters\n",
    "\n",
    "score = []\n",
    "knn_all = dict()\n",
    "for i in range(4, 16):\n",
    "    knn = KMeans(n_clusters=i, algorithm='full', tol=1e-7, random_state=42)\n",
    "    knn_all[i] = knn\n",
    "    score.append(\n",
    "        knn.fit(scaled_spectra).score(scaled_spectra)\n",
    "    )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_line(score, fig_kwargs={'width': 600, 'height': 400, 'x_axis_label': '# of clusters',\n",
    "                             'y_axis_label': 'score', 'title': 'elbow plot'});"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Find clusters (n = 8)\n",
    "\n",
    "From the plot above we can see that setting more than 8 clusters does not improve the fit much, so we are going to use 8 clusters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "knn = knn_all[8]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "knn.fit(scaled_spectra)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We prepare a map that shows the categories (labels) the algorithm found ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig_map = figure(match_aspect=True, width=350, height=500, title='categorical map',\n",
    "                 x_axis_label='x', y_axis_label='y')\n",
    "fig_map.add_tools(HoverTool(tooltips=[('label', '@image')]))\n",
    "fig_map.image([knn.labels_.reshape(64, 32)], 0, 0, 31, 63, palette=Category20[8])\n",
    "\n",
    "show(fig_map)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... and use `pandas` to assign a label to each spectrum ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spectra_df = spectra_df.assign(label=knn.labels_)\n",
    "spectra_df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... and average the spectra for each category:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mean_spectra_df = spectra_df.groupby('label').mean().drop(['x', 'y'], axis=1).T\n",
    "mean_spectra_df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we plot the averaged spectra next to the categorical map. You can click the labels of the spectra to hide them and compare different categories more easily. We export the plot to an html file (in a real project, the html could become part of the supplement of an associated paper)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig_spec = figure(width=650, height=400, title='spectra averaged over category',\n",
    "                  x_axis_label='wavelength / µm', y_axis_label='rel. intensity')\n",
    "\n",
    "for label in [6, 0, 4, 2, 5, 3, 7, 1]:\n",
    "    fig_spec.line(x=wavelength, y=mean_spectra_df[label],\n",
    "                  color=Category20[8][label],\n",
    "                  legend_label=f'label {label}', line_width=2)\n",
    "fig_spec.legend.click_policy = \"hide\"\n",
    "\n",
    "fig_map.height = 400\n",
    "fig_map.width = 300\n",
    "    \n",
    "layout = gridplot([[fig_map, fig_spec]])\n",
    "show(layout)\n",
    "\n",
    "save(layout, filename='figures/categorical_spectra.html', title='Titan spectra by category');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also create a static matplotlib figure for a report we prepare in the next notebook:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# we make our own colormap for the Bokeh colors, to make the two figures match\n",
    "cmap = ListedColormap([hex2color(c) for c in Category20[8]], name='Category20_8')\n",
    "\n",
    "fig_spec_pyplot = plt.figure(dpi=100, figsize=(8, 7))\n",
    "fig_spec_pyplot.tight_layout()\n",
    "ax_left = plt.subplot2grid((2, 3), (0, 0), colspan=1)\n",
    "ax_right = plt.subplot2grid((2, 3), (0, 1), colspan=2)\n",
    "ax_bottom = plt.subplot2grid((2, 3), (1, 0), colspan=3)\n",
    "\n",
    "plt.sca(ax_left)\n",
    "img = ax_left.imshow(knn.labels_.reshape(64, 32), cmap=cmap)\n",
    "plt.colorbar(img, label='category', ax=ax_right)\n",
    "plt.xlabel('x position')\n",
    "plt.ylabel('y position')\n",
    "plt.title('categorical map')\n",
    "\n",
    "# we make an extra plot to show the methane band at 3.3 µm in more detail\n",
    "plt.sca(ax_right)\n",
    "plt.grid(True, color=[0.9, 0.9, 0.9])\n",
    "for i in range(8):\n",
    "    plt.plot(wavelength, mean_spectra_df[i],\n",
    "             color=cmap(i), alpha=0.75, linewidth=1)\n",
    "plt.xlim([3, 3.7])\n",
    "plt.ylim([0, 0.025])\n",
    "plt.xlabel('wavelength/µm')\n",
    "plt.ylabel('rel. intensity')\n",
    "plt.title('methane at 3.3 µm')\n",
    "\n",
    "plt.sca(ax_bottom)\n",
    "plt.xticks(np.arange(1, 5.5, 0.5))\n",
    "plt.grid(True, color=[0.9, 0.9, 0.9])\n",
    "for i in range(8):\n",
    "    plt.plot(wavelength, mean_spectra_df[i],\n",
    "             color=cmap(i), alpha=0.75, linewidth=1)\n",
    "plt.xlabel('wavelength/µm')\n",
    "plt.ylabel('rel. intensity')\n",
    "plt.title('overview')\n",
    "\n",
    "fig_spec_pyplot.tight_layout()\n",
    "fig_spec_pyplot.savefig('figures/categorical_spectra.png')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plot groups with reduced dimensionality\n",
    "\n",
    "We use principle component analysis [(see comment below)](#pca) <a id=\"pca-rev\"></a> to visualize the clusters that the k-means algorithm found."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pca = PCA(n_components=6)\n",
    "reduced_spectra = pca.fit_transform(scaled_spectra)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "figs = []\n",
    "for i, j in combinations(range(6), 2):\n",
    "    fig = figure(width=200, height=200, x_axis_label=f'pc {i+1}',\n",
    "                 y_axis_label=f'pc{j+1}')\n",
    "    fig.scatter(x=reduced_spectra[:,i], y=reduced_spectra[:,j],\n",
    "                color=np.array(Category20[8])[knn.labels_], alpha=0.5,\n",
    "                line_width=0)\n",
    "    figs.append(fig)\n",
    "    \n",
    "layout = gridplot(figs, ncols=4)\n",
    "show(layout)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We make another matplotlib figure for our report:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "colors = [hex2color(c) for c in np.array(Category20[8])[knn.labels_]]\n",
    "\n",
    "fig_pca_pyplot = plt.figure(dpi=100, figsize=(8, 8.4))\n",
    "\n",
    "for k, (i, j) in enumerate(combinations(range(6), 2)):\n",
    "    plt.subplot(4, 4, k+1)\n",
    "    plt.grid(True, color=[.9, .9, .9], linestyle='dashed')\n",
    "    plt.gca().set_axisbelow(True)  # put grid in the background\n",
    "    plt.scatter(reduced_spectra[:,i], reduced_spectra[:,j],\n",
    "                color=colors, alpha=0.3, linewidth=0, marker='.')\n",
    "    plt.title(f'PC{i+1} vs PC{j+1}')\n",
    "    \n",
    "fig_pca_pyplot.tight_layout()\n",
    "fig_pca_pyplot.savefig('figures/pca.png')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"pca\"></a> \n",
    "\n",
    "# Comment on PCA\n",
    "\n",
    "To understand what principle component analysis (PCA) does, consider the following:\n",
    "\n",
    "The k-means algorithm finds clusters using all 256 channels of the VIMS instrument, i.e. the clusters exist in a 256-dimensional space which we cannot visualize directly. We could make pairwise scatter plots of two channels and hope to see the clusters but plotting principle components is a better way.\n",
    "\n",
    "What PCA does is that it finds linear combinations of the input data vectors (in our case channels) such that the resulting combinations, the *principle components*, are orthogonal and ordered by variance. Ordered by variance means that the first principle component carries the most information and has the highest potential of differentiating two entries in the dataset (spectra in our case). Depending on the dataset, it may be okay to look only at the first couple of principle components and thereby greatly reduce the complexity of the data analysis.\n",
    "\n",
    "In our case, neighboring channels are necessarily strongly correlated and there may in fact be only a few spectral features that change simultaneously. Therefore, we would not expect all principle components to carry significant information. We can see that this is true by the plots of the higher principle components above (e.g. PC5 vs. PC6, which is essentially noise).\n",
    "\n",
    "In the plots above, you can also see that the spread of the data is indeed largest in PC1, then PC2, and so on.\n",
    "\n",
    "[&uarr; back](#pca-rev)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; to top](#Classifying-the-spectra) | [next notebook &rarr;](4_report.ipynb)"
   ]
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "sample-project",
   "language": "python",
   "name": "sample-project"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
