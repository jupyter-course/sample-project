{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Cleaning the data\n",
    "\n",
    "In the [previous notebook](1_data_exploration.ipynb), we saw that some channels of Cassini VIMS instrument where obviously broken (for example 3.87 µm and 4.78 µm), that is, the spectral image revealed only noise for that wavelengths.\n",
    "\n",
    "In this notebook, we are going to clean the data by finding the broken channels and replace the data using a linear interpolation of neighboring channels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load snippets/default_import.py\n",
    "import h5py\n",
    "import numpy as np\n",
    "\n",
    "from bokeh.io import output_notebook\n",
    "from scipy.signal import find_peaks\n",
    "\n",
    "from scripts.io import load_spectra\n",
    "from scripts.visualization import plot_line\n",
    "\n",
    "output_notebook()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wavelength, spectra = load_spectra()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We start by looking at the **variance** of each channel calculated from all spectra:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "variance = np.var(spectra, axis=(1, 2))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_line(variance, fig_kwargs=dict(title='Variance vs. spectral channel'));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Not surprisingly, the channels with the highest intensity variations also have the highest variance, so this does not help us very much. However, we can already see some irregularities for the channels 234 and 253, and perhaps some others, already."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we assume that the spectra vary slowly from pixel to pixel in the image, we can find a different measure of how \"noisy\" a channel is by calculating the **variance of the spectral gradient** along the x and y directions of the image. In this way, the contribution of spectral signals to the variance should largely cancel, an we should isolate the broken channels much better. Let's see:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diff_variance_x = np.var(np.diff(spectra, axis=1), axis=(1, 2))\n",
    "diff_variance_y = np.var(np.diff(spectra, axis=2), axis=(1, 2))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# we plot the sum of the variances as a measure of noise\n",
    "plot_line(diff_variance_x+diff_variance_y,\n",
    "          fig_kwargs=dict(title='Variance of spectral gradient vs. channel'));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Indeed, now we can see the broken channels very cleary as sharp spikes in the variance of the spectral gradient. Some smaller peaks may indicate further channels which have problems, but we will focus on the four channels with the highest noise only.\n",
    "\n",
    "We can select the peaks using peak picking ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "peak_idxs, _ = find_peaks(diff_variance_x+diff_variance_y, height=1e-4)\n",
    "peak_idxs = peak_idxs[peak_idxs > 150]\n",
    "peak_idxs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... and remove them by linear interpolation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cleaned_spectra = spectra.copy()\n",
    "\n",
    "for i in range(64): # for each pixel in the spectral image (64 x 64)\n",
    "    for j in range(64):\n",
    "        for pi in peak_idxs:\n",
    "            # by manipulating a view, we manipulate the source array (here `cleaned_spectra`)\n",
    "            spectrum = cleaned_spectra[:, i, j].view()\n",
    "            spectrum[pi] = (spectrum[pi-1] + spectrum[pi+1])/2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's check the result by plotting our noise measure once more for the cleaned data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cleaned_diff_variance_x = np.var(np.diff(cleaned_spectra, axis=1), axis=(1, 2))\n",
    "cleaned_diff_variance_y = np.var(np.diff(cleaned_spectra, axis=2), axis=(1, 2))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_line(cleaned_diff_variance_x + cleaned_diff_variance_y,\n",
    "          fig_kwargs=dict(title='Variance of spectral gradient vs. channel (cleaned spectra)'));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The sharp spikes are gone and we can call this a success.\n",
    "\n",
    "We save the cleaned spectra as a new hdf file for later analysis:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with h5py.File('../data/processed/C1554970778_1_cleaned.h5', 'w') as h5file:\n",
    "    h5file.create_dataset('wavelengths', data=wavelength)\n",
    "    h5file.create_dataset('spectra', data=cleaned_spectra)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; to top](#Cleaning-the-data) | [next notebook &rarr;](3_classification.ipynb)"
   ]
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "sample-project",
   "language": "python",
   "name": "sample-project"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
