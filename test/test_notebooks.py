import nbformat
import os
import subprocess
import unittest
import sys

from context import PROJ_DIR
from functools import reduce
from nbconvert.preprocessors import ExecutePreprocessor


KERNEL = "sample-project"


def run_notebook(notebook_path):
    nb = nbformat.read(notebook_path, as_version=4)
    ep = ExecutePreprocessor(kernel_name=KERNEL)
    notebook_dir = os.path.join(PROJ_DIR, "notebooks")
    # metadate declares _where_ to execute notebook
    ep.preprocess(nb, {'metadata': {'path': notebook_dir}})


class TestEvaluationNotebooks(unittest.TestCase):

    DIR = os.path.join(PROJ_DIR, "notebooks")

    def _test_notebook_runs_wo_error(self, notebook_path):
        run_notebook(notebook_path)

    def test_executes_wo_error_evaluation(self):
        for fn in ("0_convert_VIMS_files.ipynb",
                   "1_data_exploration.ipynb",
                   "2_preprocessing.ipynb",
                   "3_classification.ipynb",
                   "4_report.ipynb"
        ):
            print(f"Running {fn}")
            fp = os.path.join(self.DIR, fn)
            with self.subTest(msg=f"running {fn}"):
                self._test_notebook_runs_wo_error(fp)


if __name__ == '__main__':
    print(f"Running notebooks using kernel '{KERNEL}'")
    unittest.main()
