import os
import unittest

from hashlib import sha1
from context import PROJ_DIR

class TestDataIngrity(unittest.TestCase):
    
    filepath = os.path.join(PROJ_DIR, "data", "converted", "C1554970778_1.h5")

    def test_data_integrity(self):
        with open(self.filepath, "rb") as f:
            self.assertEqual(
                sha1(f.read()).hexdigest(),
                '5cc16f6f1f00c3c226dfb7b2bc4856c7fbc8bc99'
            )


if __name__ == '__main__':
    unittest.main()
